import Tkinter as tk
import Tkconstants as tkc     
# print dir(tk.OptionMenu)

class My_App:
	def __init__(self):
		self.root=tk.Tk()
		self.initialize_widgets()
	def hide_right_Frame(self):
		print "hello world!"
		self.right_frame.pack_forget()
	def display_right_Frame(self):
		self.right_frame.pack()
	def initialize_widgets(self):
		recessed_frame=tk.Frame(master=self.root,relief=tkc.SUNKEN,borderwidth='1')
		# print recessed_frame.config().keys()
		# print dir(tkc)
		
		self.generate_left_frame(recessed_frame).pack(side=tkc.LEFT)
		self.generate_right_frame(recessed_frame).pack(side=tkc.LEFT)
		recessed_frame.pack()
		# #reflection
		# print label_2.config().keys()
		# print tkc.BOTTOM
	def generate_left_frame(self,parent):
		left_frame=tk.Frame(master=parent,relief=tkc.RAISED,borderwidth='2')
		tk.Button(master=left_frame,command=self.hide_right_Frame,text="hello_world",font="Courier 20 bold").pack(side='bottom')
		tk.Button(master=left_frame,command=self.display_right_Frame,text="hello world again").pack(side=tkc.BOTTOM)
		tk.Button(master=left_frame, text="display",command=self._display_slider_value).pack()
		return left_frame

	def generate_right_frame(self,parent):
		self.right_frame=tk.Frame(master=parent,relief=tkc.RAISED,borderwidth='2')
		tk.Label(master=self.right_frame,text="hello world right1!").pack()
		tk.Label(master=self.right_frame,text="hello world right2!").pack()
		self.__spinbox=tk.Spinbox(master=self.right_frame,text="spinbox",values=range(1,20),command=self.__print_spinbox)
		self.__spinbox.pack()
		self.__scale=tk.Scale(master=self.right_frame,from_=0.0, to=5.0, resolution=0.1,orient=tkc.HORIZONTAL)
		self.__scale.pack()
		print tk.Spinbox().config().keys()
		return self.right_frame
	def _display_slider_value(self): 
		print self.__scale.get()
	def __print_spinbox(self): 
		print self.__spinbox.get()

	def start_application(self):
		self.root.mainloop()

if __name__=='__main__':
	app=My_App()
	app.start_application()
